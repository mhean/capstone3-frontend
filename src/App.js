import React from 'react';
import {
  HashRouter,
  Route,
  Switch
} from 'react-router-dom';
import { css } from '@emotion/core';
import PulseLoader from 'react-spinners/PulseLoader';

const override = css`
    display: block;
    margin: 0 auto;
    border-color: #bd9317;
`;

const loading = () => {


  return (
     <div className='sweet-loading vh-100 vw-100 d-flex align-items-center justify-content-center'>
        <PulseLoader
          css={override}
          sizeUnit={"px"}
          size={50}
          color={'#fd79a8'}
          loading={loading}
        />
      </div>
  )
}

// 
const Register = React.lazy(()=>import('./views/Pages/Register'));
const Login = React.lazy(()=>import('./views/Pages/Login'));
const Schedules = React.lazy(()=>import('./views/Schedule/Schedules'));
const Landing = React.lazy(()=>import('./views/Layout/Landing'))
const Accomodation = React.lazy(()=>import('./views/Layout/Accomodation'))
const Home1 = React.lazy(()=>import('./views/Layout/Home1'))
const Payments = React.lazy(()=>import('./views/Payments/Payments'))
const Bookings = React.lazy(()=>import('./views/Booking/Bookings'))


const App = () => {
  return (
    <HashRouter>
      <React.Suspense fallback={loading()}>
        <Switch>
          <Route 
            path="/register"
            exact
            name="Register"
            render={props => <Register {...props} />}
          />
          <Route 
            path="/login"
            exact
            name="Login"
            render={props => <Login {...props}/>}
          />
          <Route 
            path="/schedules"
            exact
            name="Schedules"
            render={props=> <Schedules {...props} />}
          />
          
          <Route 
            path="/"
            exact
            name="Landing"
            render={props=><Landing />}
          />
          <Route 
            path="/home1"
            exact
            name="Home1"
            render={props=><Home1 />}
          />
          <Route 
            path="/accomodation"
            exact
            name="Accomodation"
            render={props=><Accomodation />}
          />
          
          <Route 
            path="/payments"
            exact
            name="Payments"
            render={props=><Payments />}
          />
          <Route 
            path="/bookings"
            exact
            name="Bookings"
            render={props=><Bookings />}
          />
          
        </Switch>
      </React.Suspense>
    </HashRouter>
  )
}

export default App;

