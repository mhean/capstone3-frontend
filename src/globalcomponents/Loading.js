import React, {useState} from 'react';
import { css } from '@emotion/core';
import ClipLoader from 'react-spinners/ClipLoader';

const override = css`
    display: block;
    margin: 0 auto;
    border-color: #fd79a8;
`;

const Loading = () => {


  return (
     <div className='sweet-loading vh-100 vw-100 d-flex align-items-center justify-content-center'>
        <ClipLoader
          css={override}
          sizeUnit={"px"}
          size={300}
          color={'#fd79a8'}
          loading={Loading}
        />
      </div>
  )
}

export default Loading;