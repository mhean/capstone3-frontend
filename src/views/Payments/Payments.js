import React from 'react';
import {Elements, StripeProvider} from 'react-stripe-elements';
import CheckoutForm from './CheckoutForm';
// import {Navbar, Footer} from "../Layout";

const Payments = () => {
	return (
		<React.Fragment>
	
		<StripeProvider apiKey="pk_test_yGjNcPCh15IrqUgLDy4ioZ8W00cChy9g4G">
			<div className="payment-container">

				<div className="payment d-flex justify-content-center">
					<Elements>
						<CheckoutForm />
					</Elements>
				</div>
			</div>
		</StripeProvider>

		</React.Fragment>
	)
}

export default Payments;