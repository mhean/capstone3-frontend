import React from 'react';
import {CardElement, injectStripe} from 'react-stripe-elements';
import axios from 'axios';
import Swal from 'sweetalert2'

const CheckoutForm = (props) => {
	const base_url='https://evening-lake-82728.herokuapp.com';
	// const base_url='http://localhost:4000';
		// CommonJS
	const Swal = require('sweetalert2')

	const submit = async (e) => {
		let user = JSON.parse(sessionStorage.user);
		
		let {token} = await props.stripe.createToken({name:"Name"})

		axios.post(base_url+'/charge',{
			email: user.email,
			amount: (props.toPay)
			

		}).then(Swal.fire({
			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Pay Now!'
		}).then((result) => {
			if (result.value) {
				Swal.fire(
					'Success!',
					'Checkout Complete.',
					'success'
					)
			}
		}));

		
	}
	// console.log(props.toPay)
	return (
		<div className="checkout text-center">
			<h5> P{props.toPay}.00</h5>
	       	<p className="py-3">Would you like to complete the payment?</p>
        	<CardElement py-2 />
        	<button 
        	className="btn btn-primary my-3 col-lg-12" 
        	style={{"backgroundColor":"#fd79a8", "color": "#fff","border": "1px solid #fd79a8"}}
        	onClick={submit}>Pay Now</button>
      	</div>
	)
}

export default injectStripe(CheckoutForm);



