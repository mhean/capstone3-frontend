import React from 'react';
import {Elements, StripeProvider} from 'react-stripe-elements';
import CheckoutForm from './CheckoutForm';
import {Navbar, Footer} from "../Layout";

const Payments = () => {
	return (
		<React.Fragment>
		<Navbar/>
		<StripeProvider apiKey="pk_test_uIeipTDzXlSjf5httG1aaWS500NWxcts3c">
			<div className="payment-container">

				<h1 className="text-center py-5">Payment</h1>
				<div className="payment d-flex justify-content-center">
					<Elements>
						<CheckoutForm />
					</Elements>
				</div>
			</div>
		</StripeProvider>
		<Footer/>
		</React.Fragment>
	)
}

export default Payments;