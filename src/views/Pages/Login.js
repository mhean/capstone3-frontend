import React, {useState} from 'react';
import {FormInput} from '../../globalcomponents'
import {Button} from 'reactstrap'
import axios from 'axios'
// import {NavbarMain} from '../Layout';
import swal from 'sweetalert';

const Login = () => {
	const base_url='https://evening-lake-82728.herokuapp.com';
	// const base_url='http://localhost:4000';
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const handleEmailChange = e => {
		setEmail(e.target.value);
		// console.log(e.target.value)

	}
	const handlePasswordChange = e => {
		setPassword(e.target.value);
		// console.log(e.target.value)
	}

	const handleLogin = () => {
		axios.post(base_url+"/login",{
			email,
			password
			
		}).then(res=>{
			sessionStorage.token = res.data.token;
			sessionStorage.user = JSON.stringify(res.data.user)
			
			// console.log(res.data.token)

			window.location.replace('#/schedules')
			swal("", "Successfully login!", "success");
		})
	}

	return (
		<React.Fragment>
		
		<div className="container-Log" style={{"color": "#fff"}}>
			<div
				className="login-container col-lg-4 offset-lg-4"
			>
			<h3 className="login-text text-center">Login</h3>
			<FormInput 
				// className="label"
				label={"Email"}
				placeholder={"Enter your email"}
				required
				type={"email"}
				onChange={handleEmailChange}
			/>
			<FormInput 
				// style={{"color": "#fff"}}
				// id="label"
				label={"Password"}
				placeholder={"Enter your password"}
				required
				type={"password"}
				onChange={handlePasswordChange}
			/>
			<Button
				block
				style={{backgroundColor:"#fd79a8",color:"#fff", border:"#ffcccc"}}
				className="btn-login mt-5"
				onClick={handleLogin}
			>
				Login
			</Button>
			<p className="mt-3" style={{color:"#fd79a8"}}>Not yet registered?</p>
				<a className="d-flex" href="#/register" style={{color:"#fd79a8"}}>Register here!</a>
			</div>
		</div>
		</React.Fragment>
		)
}

export default Login;