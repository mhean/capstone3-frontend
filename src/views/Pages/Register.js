import React, {useState} from 'react';
import {FormInput} from '../../globalcomponents';
import {Button} from 'reactstrap';
import axios from 'axios';
// import {NavbarMain} from '../Layout';
import swal from 'sweetalert';


const Register = () => {
	

	const base_url='https://evening-lake-82728.herokuapp.com';
	// const base_url='http://localhost:4000';
	const [name, setName] = useState("");
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const handleNameChange = (e) => {
		setName(e.target.value)
		// console.log(e.target.value)
	}

	const handleEmailChange = (e) => {
		setEmail(e.target.value);
		// console.log(e.target.value)
	}

	const handlePasswordChange = e => {
		setPassword(e.target.value);
		// console.log(e.target.value);
	}

	const handleRegister = () => {
		axios.post(base_url+"/register", {
			name: name,
			email: email,
			password: password
		}).then(res=>console.log(res.data))
		// swal("", "Successfully Registered!", "success");
		window.location.replace('#/login')
		
	}

	return (

		<React.Fragment>
		
			<div className="container-reg">
			<div className="register-container col-lg-4 offset-lg-4">
				<h3 className="register-text text-center">Register</h3>
				<FormInput 
					required={"true"}
					label={"Name"}
					placeholder={"Enter your name"}
					type={"text"}
					onChange={handleNameChange}q 														
				/>
				<FormInput 
					label={"Email"}
					placeholder={"Enter your email"}
					type={email}
					onChange={handleEmailChange}
				/>
				<FormInput 
					label={"Password"}
					placeholder={"Enter your password"}
					type={"password"}
					onChange={handlePasswordChange}
				/>
				<Button
					block
					style={{backgroundColor:"#fd79a8",color:"#fff", border:"#ffcccc"}}
					className="btn-register mt-5"
					onClick={handleRegister}
				>
					Register
				</Button>
				<p className="mt-3" style={{color:"#fd79a8"}}>Already registered?</p>
				<a className="d-flex" href="#/login" style={{color:"#fd79a8"}}> Login here!</a>
			</div>
		</div>
		</React.Fragment>

	)
}

export default Register;
