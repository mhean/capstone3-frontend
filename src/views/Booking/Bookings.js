import React, {useEffect, useState} from 'react';
import CsvDownload from 'react-json-to-csv';
import swal from 'sweetalert';
import axios from 'axios';
import {BookList} from './components';

import {
	// Col,
	Row,
	Container
} from 'reactstrap'
// import moment from 'moment'
import {Navbar,Footer} from '../Layout'

const Bookings = () => {
	const base_url='https://evening-lake-82728.herokuapp.com';
	// const base_url='http://localhost:4000';
	const [bookings, setBookings] = useState([])
	const [user, setUser] = useState({})
	// console.log(user)

	const [isAdmin, setIsAdmin] = useState(false);
	
	useEffect(()=>{
		let user = JSON.parse(sessionStorage.user);
		setIsAdmin(user.isAdmin);
	},[]);
	
	
	
	useEffect(()=>{
		
		if(sessionStorage.token){
			let user = JSON.parse(sessionStorage.user)
			// console.log(user)
			if(user.isAdmin){
				axios.get(base_url+'/showbookings').then(res=>{
				setBookings(res.data)
				})
			}else{
				axios.get(base_url+'/showbookingsbyuser/'+user.id).then(res=>{
					setBookings(res.data)
					// console.log(res.data)
				})
			}

			setUser(user);

		}else{
			window.location.replace('#/login')
		}

	}, []);

	const handleDeleteBooking = (bookingId) => {
		axios.delete(base_url+'/deletebooking/'+bookingId).then(res=>{
			let index = bookings.findIndex(booking=>booking._id===bookingId);
			let newBookings = [...bookings];
			newBookings.splice(index,1);
			setBookings(newBookings);
			
		})
		swal("", "Booking successfully deleted!", "success");
	}


	return (
		<React.Fragment>
		<Navbar />
		
			<div className="thumbnail">
			
			<Container className="py-5">
			<div>
				<h1 
				className="text-center py-1">{isAdmin ? "Bookings" : "My Bookings"}</h1>
				
			</div>
			<CsvDownload data={bookings} 
				style={{"backgroundColor":"#fd79a8", "color": "white"}}
			/>
				<Row>
					
					{bookings.map(booking=>(
						<BookList 
							key={booking._id}
							booking={booking}
							handleDeleteBooking={handleDeleteBooking}
							user={user}
							
						/>
					))}
			
				
				</Row>
				</Container>
				</div>
			<Footer/>
		</React.Fragment>
	)

}
export default Bookings;