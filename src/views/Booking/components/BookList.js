import React, {useState,useEffect} from 'react';
import CsvDownload from 'react-json-to-csv';
import axios from 'axios';

// import {Details} from '../components';
import {
	Button, 
	Col, 
	Card, 
	CardBody, 
	CardTitle, 
	CardSubtitle, 
	CardText,
	Row,
	Container
} from 'reactstrap'
import { userInfo } from 'os';


const BookList = (props) => {

	// const [showDetails, setShowDetails] = useState(false);

	const booking = props.booking;
	const [isAdmin, setIsAdmin] = useState(false);
	
	useEffect(()=>{
		let user = JSON.parse(sessionStorage.user);
		setIsAdmin(user.isAdmin);
	},[]);
	
	return (
		<React.Fragment>
		<Col lg={6}

		 className="mt-5">
			
			<Card>
	
				<CardBody className="bookListCard">
				
				<CardTitle>{booking.vesselName}</CardTitle>
				<CardTitle>{booking.departurePier} - {booking.arrivalPier}</CardTitle>
				<CardTitle>{booking.departureDate}({booking.departureDay}) - {booking.arrivalDate}({booking.arrivalDay})</CardTitle>
				<CardTitle>{booking.departureTime} - {booking.arrivalTime}</CardTitle>
				<CardTitle>{booking.accomodationType} for {booking.quantity} person </CardTitle>	
				<CardTitle>P{booking.totalPayment}.00</CardTitle>	
				<CardTitle>Ticket Number: {booking._id}</CardTitle>	
				{/* {isAdmin ?
				<CardTitle>Booked By: {props.user.name}</CardTitle>	
				: ""}	 */}
					<Button
					color="danger"
					className="col-lg-12"
					onClick={()=>props.handleDeleteBooking(booking._id)}
					>
					Delete Booking
					</Button>
					
					<CardText className="mt-2">Booked {booking.createdAt}</CardText>
				</CardBody>
			</Card>
		</Col>
		
		</React.Fragment>
	)
}

export default BookList;