import React, {useState, useEffect} from 'react';
import {ScheduleRow, ScheduleForm,BookForm} from './components';

import {Button} from 'reactstrap';
import axios from 'axios';
import {Navbar,Footer} from "../Layout";
import swal from 'sweetalert';

const Schedules = ()=>{
	const base_url='https://evening-lake-82728.herokuapp.com';
	// const base_url='http://localhost:4000';
	const [schedule, setSchedule]= useState([]);
	const [schedules, setSchedules] = useState([]);
	const [showScheduleForm, setShowScheduleForm]= useState(false);
	const [vesselName, setVesselName]= useState("");
	const [vesselNameRequired, setVesselNameRequired]= useState(true);
	const [departureDate, setDepartureDate]= useState("");
	const [departureDateRequired, setDepartureDateRequired]= useState(true);
	const [departureDay, setDepartureDay]= useState("");
	const [departureDayRequired, setDepartureDayRequired]= useState(true);
	const [departureTime, setDepartureTime]= useState("");
	const [departureTimeRequired, setDepartureTimeRequired]= useState(true);
	const [departurePier, setDeparturePier]= useState("");
	const [departurePierRequired, setDeparturePierRequired]= useState(true);

	const [arrivalDate, setArrivalDate]= useState("");
	const [arrivalDateRequired, setArrivalDateRequired]= useState(true);
	const [arrivalDay, setArrivalDay]= useState("");
	const [arrivalDayRequired, setArrivalDayRequired]= useState(true);
	const [arrivalTime, setArrivalTime]= useState("");
	const [arrivalTimeRequired, setArrivalTimeRequired]= useState(true);
	const [arrivalPier, setArrivalPier]= useState("");
	const [arrivalPierRequired, setArrivalPierRequired]= useState(true);
	const [showBookForm, setShowBookForm]=useState(false);
	const [booking, setBooking]=useState("");
	const [userName, setUserName]=useState("");
	const [scheduleCode, setScheduleCode]=useState("");
	const [paymentStatus, setPaymentStatus]=useState("");
	const [paymentMethod, setPaymentMethod]=useState("");
	const [user, setUser] = useState({})
	const [bookingDetails, setBookingDetails]=useState({});

	const [isAdmin, setIsAdmin] = useState(false);
	// const user = props.user;
	
	useEffect(()=>{
		let user = JSON.parse(sessionStorage.user);
		setIsAdmin(user.isAdmin);
		setUser(user);
	},[]);
	// console.log(user)

	const handleBookingDetails=(scheduleId)=>{
		
		axios.get(base_url+"/admin/showschedulebyid/" + scheduleId
		).then(res=>{
			// console.log(res.data)
			setBookingDetails(res.data);
			setShowBookForm(true);

		})
	}

	const handleShowScheduleForm=()=>{
		setShowScheduleForm(!showScheduleForm)
	}

	const handleShowBookForm= ()=>{
		setShowBookForm(!showBookForm)
	}

	const handleVesselNameChange=(e)=>{
		if(e.target.value==""){
			setVesselNameRequired(true);
			setVesselName("");
		}else{
			setVesselNameRequired(false)
			setVesselName(e.target.value);
		}
	}

	const handleDepartureDateChange=(e)=>{
		if(e.target.value==""){
			setDepartureDateRequired(true);
			setDepartureDate("");
		}else{
			setDepartureDateRequired(false)
			setDepartureDate(e.target.value);
		}
	}
	const handleDepartureDayChange=(e)=>{
		if(e.target.value==""){
			setDepartureDayRequired(true);
			setDepartureDay("");
		}else{
			setDepartureDayRequired(false)
			setDepartureDay(e.target.value);
		}
	}
	const handleDepartureTimeChange=(e)=>{
		if(e.target.value==""){
			// setDepartureTimeRequired(true);
			setDepartureTime("");
		}else{
			// setDepartureTimeRequired(false)
			setDepartureTime(e.target.value);
		}
	}
	const handleDeparturePierChange=(e)=>{
		if(e.target.value==""){
			setDeparturePierRequired(true);
			setDeparturePier("");
		}else{
			setDeparturePierRequired(false)
			setDeparturePier(e.target.value);
		}
	}
	const handleArrivalDateChange=(e)=>{
		if(e.target.value==""){
			setArrivalDateRequired(true);
			setArrivalDate("");
		}else{
			setArrivalDateRequired(false)
			setArrivalDate(e.target.value);
		}
	}
	const handleArrivalDayChange=(e)=>{
		if(e.target.value==""){
			setArrivalDayRequired(true);
			setArrivalDay("");
		}else{
			setArrivalDayRequired(false)
			setArrivalDay(e.target.value);
		}
	}
	const handleArrivalTimeChange=(e)=>{
		if(e.target.value==""){
			setArrivalTimeRequired(true);
			setArrivalTime("");
		}else{
			setArrivalTimeRequired(false)
			setArrivalTime(e.target.value);
		}
	}
	const handleArrivalPierChange=(e)=>{
		if(e.target.value==""){
			setArrivalPierRequired(true);
			setArrivalPier("");
		}else{
			setArrivalPierRequired(false)
			setArrivalPier(e.target.value);
		}
	}


	const handleRemoveSchedule = scheduleId => {
		
		axios({
			method: "DELETE",
			url: base_url+"/admin/deleteschedule/" + scheduleId
		})
		
		.then(res=>{
			let newSchedules = schedules.filter(schedule=>schedule._id!==scheduleId);
			setSchedules(newSchedules)
		})
		.then((willDelete) => {
		  swal("", "Schedule has been deleted!", "success");
		});
	}

	const handleSaveSchedule = () => {
		axios({
			method: "POST",
			url:base_url+"/admin/addschedule",
			data:{
				vesselName:vesselName,
				departureDate:departureDate,
				departureDay:departureDay,
				departureTime:departureTime,
				departurePier:departurePier,

				arrivalDate:arrivalDate,
				arrivalDay:arrivalDay,
				arrivalTime:arrivalTime,
				arrivalPier:arrivalPier
			}

		}).then(res=>{
			let newSchedules=[...schedules];
			newSchedules.push(res.data);
			setSchedules(newSchedules);
		});

		handleRefresh();
		swal("", "New Schedule has been added!", "success");
	}

	const handleRefresh = ()=>{
		setShowScheduleForm(false);
		setVesselName("")
		setDepartureDate("")
		setDepartureDay("")
		setDepartureTime("")
		setDeparturePier("")
		setVesselNameRequired(true)
		setDepartureDateRequired(true)
		setDepartureDayRequired(true)
		setDepartureTimeRequired(true)
		setDeparturePierRequired(true)

		setArrivalDate("")
		setArrivalDay("")
		setArrivalTime("")
		setArrivalPier("")
		setArrivalDateRequired(true)
		setArrivalDayRequired(true)
		setArrivalTimeRequired(true)
		setArrivalPierRequired(true)
		
	}

	

	useEffect(()=>{
		// if(sessionStorage.token){
		// 	let user = JSON.parse(sessionStorage.user);
		// 	if(user.isAdmin){
				axios.get(base_url+'/admin/showschedule'
				).then(res=>{
				setSchedules(res.data);
			});
		// 		setUser(user);

		// 	}else{
		// 		window.location.replace('/')
		// 	}
		// }else{
		// 	window.location.replace('#/login')
		// }
		
	}, []);
	return(
		<React.Fragment>
		<Navbar/>
	
			<div className="schedules-parallax">
				
				<div className="d-flex justify-content-center  pt-5 pb-2">
				{isAdmin ?
					<Button
					className="my-1 "
					style={{"backgroundColor":"#fd79a8", "color": "#fff","border": "1px solid #fd79a8"}}
					onClick={handleShowScheduleForm}					
					>+ Add Schedule</Button>
				: ""}
					<BookForm
					schedule={schedule}
					showBookForm={showBookForm}
					handleShowBookForm={handleShowBookForm}
					handleBookingDetails={handleBookingDetails}
					bookingDetails={bookingDetails}
					user={user}
					/>

					<ScheduleForm
						
						showScheduleForm={showScheduleForm}
						handleShowScheduleForm={handleShowScheduleForm}
						vesselNameRequired={vesselNameRequired}
						handleVesselNameChange={handleVesselNameChange}
						departureDateRequired={departureDateRequired}
						handleDepartureDateChange={handleDepartureDateChange}
						departureDayRequired={departureDayRequired}
						handleDepartureDayChange={handleDepartureDayChange}
						departureTimeRequired={departureTimeRequired}
						handleDepartureTimeChange={handleDepartureTimeChange}
						departurePierRequired={departurePierRequired}
						handleDeparturePierChange={handleDeparturePierChange}

						arrivalDateRequired={arrivalDateRequired}
						handleArrivalDateChange={handleArrivalDateChange}
						arrivalDayRequired={arrivalDayRequired}
						handleArrivalDayChange={handleArrivalDayChange}
						arrivalTimeRequired={arrivalTimeRequired}
						handleArrivalTimeChange={handleArrivalTimeChange}
						arrivalPierRequired={arrivalPierRequired}
						handleArrivalPierChange={handleArrivalPierChange}
						handleSaveSchedule={handleSaveSchedule}

					/>		
				</div>
			
			<table
				className="table table-striped border col-lg-10 offset-lg-1 mb-5"
			>
				<thead>
					<tr className="text-center">Schedules</tr>
					<tr className="schedRow">
						<th className="schedHead">Vessels Name</th>
						<th className="schedHead">Departure Date</th>
						<th className="schedHead">Departure Day</th>
						<th className="schedHead">Departure Time</th>
						<th className="schedHead">Departure Pier</th>
						<th className="schedHead">Arrival Date</th>
						<th className="schedHead">Arrival Day</th>
						<th className="schedHead">Arrival Time</th>
						<th className="schedHead">Arrival Pier</th>
						<th className="schedHead">Action</th>
					</tr>
				</thead>
				<tbody>				
					{schedules.map(schedule=>
					<ScheduleRow
						key = {schedule._id}
						schedule = {schedule}
						handleRemoveSchedule = {handleRemoveSchedule}

						showBookForm={showBookForm}
						handleShowBookForm={handleShowBookForm}
						handleBookingDetails={handleBookingDetails}
						bookingDetails={bookingDetails}

					/>

				)}
				</tbody>
			</table>
			
		<Footer/>
		</div>
		</React.Fragment>
		)
}
export default Schedules;