import React from 'react';
import {Modal,ModalHeader,ModalBody,Button} from 'reactstrap';
import {FormInput} from '../../../globalcomponents';

const ScheduleForm = (props) => {

	return(
		<React.Fragment>
			<Modal
				isOpen={props.showScheduleForm}
				toggle={props.handleShowScheduleForm}
		>
			<ModalHeader
				toggle={props.handleShowScheduleForm}
				style={{"backgroundColor":"#fd79a8", "color": "white"}}
			>
			Add Schedule 
			</ModalHeader>
			<ModalBody >
				<FormInput
					label={"Vessel's Name"}
					type={"text"}
					name={"vesselName"}
					placeholder={"Enter Vessel's Name"}
					required={props.vesselNameRequired}
					onChange={props.handleVesselNameChange}
				/>
				<FormInput
					label={"Departure Date"}
					type={"date"}
					name={"departureDate"}
					placeholder={"Enter Depature Date"}
					required={props.departureDateRequired}
					onChange={props.handleDepartureDateChange}
				/>
				<FormInput
					label={"Departure Time"}
					type={"time"}					
					name={"departureTime"}
					placeholder={"Enter Departure Time"}
					required={props.departureTimeRequired}
					onChange={props.handleDepartureTimeChange}
				/>
				
				<FormInput
					label={"Departure Day"}
					type={"text"}
					name={"departureDay"}
					placeholder={"Enter Depature Day"}
					required={props.departureDayRequired}
					onChange={props.handleDepartureDayChange}
				/>

				<FormInput
					label={"Departure Pier"}
					type={"text"}
					name={"departurePier"}
					placeholder={"Enter Depature Pier"}
					required={props.departurePierRequired}
					onChange={props.handleDeparturePierChange}
				/>
				
				<FormInput
					label={"Arrival Date"}
					type={"date"}
					name={"arrivalDate"}
					placeholder={"Enter Arrival Date"}
					required={props.arrivalDateRequired}
					onChange={props.handleArrivalDateChange}
				/>
				<FormInput
					label={"Arrival Day"}
					type={"text"}
					name={"arrivalDay"}
					placeholder={"Enter Arrival Day"}
					required={props.arrivalDayRequired}
					onChange={props.handleArrivalDayChange}
				/>
				<FormInput
					label={"Arrival Time"}
					type={"time"}					
					name={"arrivalTime"}
					placeholder={"Enter Arrival Time"}
					required={props.arrivalTimeRequired}
					onChange={props.handleArrivalTimeChange}
				/>

				<FormInput
					label={"Arrival Pier"}
					type={"text"}
					name={"arrivalPier"}
					placeholder={"Enter Arrival Pier"}
					required={props.arrivalPierRequired}
					onChange={props.handleArrivalPierChange}
				/>
				
				
				<Button
					style={{"backgroundColor":"#fd79a8", "color": "#fff","border": "1px solid #fd79a8"}}
					disabled={
						props.vesselName == "" ||
						props.departureDate =="" ||
						props.departureDay == "" ||
						props.departureTime == "" ||
						props.departurePier == "" ||
						props.arrivalDate == "" ||
						props.arrivalDay == "" ||
						props.arrivalTime =="" ||
						props.arrivalPier == "" 
						? true : false}
					onClick={props.handleSaveSchedule}
		
				>Add Schedule</Button>
			</ModalBody>
		</Modal>
		</React.Fragment>
		)
}
export default ScheduleForm;