import React, {useState, useEffect} from 'react';
import {Button} from 'reactstrap';
import BookForm from './BookForm';
import axios from 'axios';

const ScheduleRow = props => {

	const schedule = props.schedule;
	const bookingDetails=props.bookingDetails;
	const [isAdmin, setIsAdmin] = useState(false);
	const user = props.user;
	
	useEffect(()=>{
		let user = JSON.parse(sessionStorage.user);
		setIsAdmin(user.isAdmin);
	},[]);
	

	return(
		<React.Fragment>
			<tr className="table-danger">
				<td>{schedule.vesselName}</td>
				<td>{schedule.departureDate}</td>
				<td>{schedule.departureDay}</td>
				<td>{schedule.departureTime}</td>
				<td>{schedule.departurePier}</td>
				<td>{schedule.arrivalDate}</td>
				<td>{schedule.arrivalDay}</td>
				<td>{schedule.arrivalTime}</td>
				<td>{schedule.arrivalPier}</td>
				<td>
					{isAdmin ?
					<Button
						className="form-control my-1"
						color="danger"				
						
						onClick={()=>props.handleRemoveSchedule(schedule._id)}
					>
						Remove
					</Button>
				:
					<Button
						className="form-control"
						style={{"backgroundColor":"#fd79a8", "color": "#fff","border": "1px solid #fd79a8"}}				
						onClick={()=>props.handleBookingDetails(schedule._id)}
					>
						Book
					</Button>
					}
				</td>

			</tr>	
		</React.Fragment>
	)
}
export default ScheduleRow;