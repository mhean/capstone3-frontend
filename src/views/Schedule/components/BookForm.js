import React, {useEffect, useState} from 'react';
import {Elements, StripeProvider} from 'react-stripe-elements';
import Swal from 'sweetalert2';
import {CheckoutForm} from '../../Payments';
import {Payments} from '../../Payments';
import {
	Modal,ModalFooter,
	ModalHeader,
	ModalBody,
	Button,
	Label,
	Dropdown,
	DropdownToggle,
	DropdownMenu,
	DropdownItem
	} from 'reactstrap';
import {FormInput} from '../../../globalcomponents';
import axios from 'axios';


const BookForm = props => {
// const base_url='http://localhost:4000';
const base_url='https://evening-lake-82728.herokuapp.com';
	const bookingDetails= props.bookingDetails;
	const user = props.user;
	// console.log(props.user)
	

	const [accomodations, setAccomodations]= useState([]);
	const [accomodationType, setAccomodationType]= useState("");
	const [dropdownOpen, setDropdownOpen] = useState(false);
	const [quantity, setQuantity]=useState(0);
	const [totalFee, setTotalFee]=useState(0);
	const [accomodationPrice, setAccomodationPrice]=useState(0);
	// const [showPaymentForm, setShowPaymentForm]= useState(false);
	const [bookings, setBookings] = useState([]);
	const [toPay, setToPay] = useState(0);

	const [modal, setModal] = useState(false);
	const [nestedModal, setNestedModal] = useState(false);
	const [closeAll, setCloseAll] = useState(false);
	
	const toggle = () => {
		setModal(!modal);
		setDropdownOpen(!dropdownOpen);

		}
	const toggleNested = () => 
	 {
	    setNestedModal(!nestedModal);
	    setCloseAll(false);
	 }

	  const toggleAll = () => 
	  {
	    setNestedModal(!nestedModal);
	    setCloseAll(true);
	  }
	  

	useEffect(()=>{
		axios.get(base_url+'/showaccomodation').then(res=>{
			setAccomodations(res.data);
		})
	}, []);

	// const handleFullNameChange =(e)=>{
	// 	setFullName(e.target.value)
	// }

	const handleSetAccomodation=(accomodation)=>{
		let totalPayment=accomodationPrice*quantity
		setAccomodationType(accomodation.name)
		setAccomodationPrice(accomodation.price)
		setTotalFee(totalPayment)
		
	}
	
	const handleQuantityChange=(e)=>{
		let quantity=(e.target.value)
		let totalPayment=accomodationPrice*quantity
		setQuantity(quantity)
		setTotalFee(totalPayment)
		setToPay(totalPayment)

	}
	const handleSaveBook=()=>{
		// let fullName= bookingDetails.fullName
		let vesselName=bookingDetails.vesselName
		let departureDate=bookingDetails.departureDate
		let departureDay=bookingDetails.departureDay
		let departurePier=bookingDetails.departurePier
		let departureTime=bookingDetails.departureTime
		let arrivalDate=bookingDetails.arrivalDate
		let arrivalDay=bookingDetails.arrivalDay
		let arrivalPier=bookingDetails.arrivalPier
		let arrivalTime=bookingDetails.arrivalTime
		let user = props.user.name
		let userId = props.user.id
		let totalPayment=totalFee
		setToPay(totalPayment);

		axios.post(base_url+'/addbooking',{
			// fullName:fullName,
			vesselName:vesselName,
			departureDate:departureDate,
			departureDay:departureDay,
			departurePier:departurePier,
			departureTime:departureTime,
			arrivalDate:arrivalDate,
			arrivalDay:arrivalDay,
			arrivalTime:arrivalTime,
			arrivalPier:arrivalPier,
			accomodationType:accomodationType,
			quantity:quantity,		
			user: user,
			userId: userId,
			totalPayment: totalPayment
		}).then(res=>{
			let newBookings = [...bookings]
			newBookings.push(res.data)
			setBookings(newBookings)
			// console.log(newBookings)
			Swal.fire({
				position: 'center',
				icon: 'success',
				title: 'Booking Successfully Save!',
				showConfirmButton: false,
				timer: 1500
			  })
			
		})
		

	}

	
	return(
		<Modal
			isOpen={props.showBookForm}
			toggle={props.handleShowBookForm}
		>
			<ModalHeader
				toggle={props.handleShowBookForm}
				style={{"backgroundColor":"#fd79a8", "color": "white"}}
			>
			Book Schedule 
			</ModalHeader>
			<ModalBody>
				<h3 className="text-center">Book Details</h3>
				<h6>Vessel's Name: {bookingDetails.vesselName}</h6>
				<h6>Departure Date: {bookingDetails.departureDate} ({bookingDetails.departureDay})</h6>
				<h6>Departure Time: {bookingDetails.departureTime}</h6>
				<h6>Departure Pier: {bookingDetails.departurePier}</h6>
				<h6>Arrival Date: {bookingDetails.arrivalDate} ({bookingDetails.arrivalDay})</h6>
				<h6>Arrival Time: {bookingDetails.arrivalTime}</h6>
				<h6>Arrival Pier: {bookingDetails.arrivalPier}</h6>
				
				<h6>Accomodation Type:</h6>
				<Dropdown
					isOpen={dropdownOpen}
					toggle={toggle}
				>
					<DropdownToggle caret>
						{accomodationType==""
						? "Choose Accomodation"
						: accomodationType}
					</DropdownToggle>
					<DropdownMenu>
						{accomodations.map(accomodation=>
							<DropdownItem
								key={accomodation._id}
								onClick={()=>handleSetAccomodation(accomodation)}
							>
								{accomodation.name}
							</DropdownItem>
						)}
					</DropdownMenu>
				</Dropdown>
				<h6 className="mt-3">Accomodation Price: P{accomodationPrice}.00</h6>
				<h6>Number of Accomodation:</h6>
				<FormInput
					// label={"Number of Accomodation"}
					type={"Number"}
					name={"quantity"}
					placeholder={"Enter number of accomodations"}
					onChange={handleQuantityChange}
				/>
				<h6>Total Fee: P{totalFee}.00</h6>

				<ModalFooter>
				<Button
						style={{"backgroundColor":"#fd79a8", "color": "#fff","border": "1px solid #fd79a8"}}
				
					onClick={handleSaveBook }
					>Save Booking</Button>
					<Button 
					style={{"backgroundColor":"#fd79a8", "color": "#fff","border": "1px solid #fd79a8"}}
					onClick={toggleNested}>Checkout<i class="fas fa-arrow-square-right"></i></Button>
					
				</ModalFooter>
				<Modal isOpen={nestedModal} toggle={toggleNested} onClosed={closeAll ? toggle : undefined}>
	            <ModalHeader>Nested Modal title</ModalHeader>
	            <ModalBody>Stuff and things</ModalBody>
	            <ModalFooter>
	              <Button 
				  style={{"backgroundColor":"#fd79a8", "color": "#fff","border": "1px solid #fd79a8"}}
	              onClick={toggleNested}
	              >Done</Button>{' '}
	              <Modal isOpen={nestedModal} toggle={toggleNested} onClosed={closeAll ? toggle : undefined}>
					<ModalHeader style={{"backgroundColor":"#fd79a8", "color": "white"}}>Stripe Payment</ModalHeader>
		            <ModalBody>
		            	<StripeProvider apiKey="pk_test_yGjNcPCh15IrqUgLDy4ioZ8W00cChy9g4G">
							<div>
								
								<div className="d-flex justify-content-center">
									<Elements>
										<CheckoutForm 
										toPay={toPay}
										
										/>
									</Elements>
								</div>
							</div>
						</StripeProvider>

		            </ModalBody>
            <ModalFooter>
              <Button  
              style={{"backgroundColor":"#fd79a8", "color": "#fff","border": "1px solid #fd79a8"}}
              onClick={toggleNested}>Done</Button>{' '}
      
            </ModalFooter>
          </Modal>
	              <Button color="secondary" onClick={toggleAll}>All Done</Button>
	            </ModalFooter>
	          </Modal>
			</ModalBody>
		</Modal>
	)
}
export default BookForm;