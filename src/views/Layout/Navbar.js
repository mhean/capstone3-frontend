import React,{useEffect,useState} from 'react';
import {Link} from 'react-router-dom';

const handleLogout = () => {
    sessionStorage.clear()
    window.location.replace('#/')
  }
  
const Navbar = () => {
	const [user, setUser] = useState({})

	useEffect(()=>{
		if(sessionStorage.token){
			let user = JSON.parse(sessionStorage.user)
			
			setUser(user);
		}else{
			console.log(user);
		}
	}, [])
	return (
		<React.Fragment>
			<nav className="navbar navbar-expand-lg">
			  <p className="blueship navbar-brand" >TravGo</p>

			  <div className="collapse navbar-collapse" id="navbarColor01">
			    <ul className="navbar-nav ml-auto">
			      <li className="nav-item active">
			        <Link to="/home1"
			        className="text-white"
			        >Home</Link>
			      </li>
			     	      
			      <li className="nav-item">
			        <Link 
			        to="/accomodation"
			        className="text-white"
			        >Accomodations</Link>
			      </li>
			      <li className="nav-item">
			        <Link 
			        to="/schedules"
			        className="text-white"
			        >Schedules</Link>
			      </li>
			       <li className="nav-item">
			        <Link 
			        to="/bookings"
			        className="text-white"
			        >Bookings</Link>
			      </li>
				  <li className="nav-item" >
			        <Link 
			        to="#"
			        className="text-white"
					// style={{"color":"red"}}
			        >Hello, {user.name}</Link>
			      </li>
				 
			      <li className="nav-item">
			        <Link to="/"
			        	className="text-white"
			        	onClick={handleLogout}
			        >Logout</Link>
			      </li>
			      
			    </ul>
			    
			  </div>
			</nav>
		</React.Fragment>
	)
}

export default Navbar;