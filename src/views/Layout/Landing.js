import React from 'react';
import {NavbarMain} from '../Layout';
import {Button} from 'reactstrap';
// import {Link} from 'react-router-dom';
// import { Player } from 'video-react';

const Landing =()=>{
	const handleBookLog =()=>{
		window.location.replace('#/register')
	}
    
        return (
           <React.Fragment>
      <NavbarMain />
        <div className="homepage d-flex justify-content-center align-items-start" style={{height:"95vh"}}>
          <h1 className="landing-text">Book the best trip of your life...</h1> 
           
        
        </div>
        <div className="btn-landing d-flex justify-content-center align-items-center">
        <Button
          onClick={handleBookLog}

          >View Schedule 
        </Button>
        </div>
      
    </React.Fragment>
        )
    


}
export default Landing;