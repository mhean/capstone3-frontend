import React from 'react';
import {Navbar,Footer} from "../Layout";
// import { Card} from 'reactstrap';
import Coverflow from 'react-coverflow';

const Accomodation = () => {
	return (
		<React.Fragment>
		<Navbar/>
			<div className="container vh-280">
				
				<h3 className="text-center py-3">Accomodations</h3>
				<Coverflow
				    width={960}
				    height={480}
				    displayQuantityOfSide={2}
				    navigation={false}
				    enableHeading={false}
				  >
				    <div
				      // onClick={() => fn()}
				      // onKeyDown={() => fn()}
				      role="menuitem"
				      tabIndex="0"
				    >
				      
				    </div>
				    <img src='./cabin.jpg' alt='title or description'/>
				   
				    <img src='./Stateroom.jpg' alt='title or description'/>
				    <img src='./Tourist.jpg' alt='title or description'/>
				    <img src='./airline-Seater.jpg' alt='title or description'/>
				    <img src='./Mega-Value.jpg' alt='title or description'/>
				    <img src='./Suite-Room.jpg' alt='title or description'/>
				  </Coverflow>, 
					<h3 className="text-center py-3">Facilities</h3>
					
			</div>
			<div className="container vh-280">
				<Coverflow
			    width={960}
			    height={480}
			    displayQuantityOfSide={2}
			    navigation={false}
			    enableHeading={false}
			  >
			    <div
			      // onClick={() => fn()}
			      // onKeyDown={() => fn()}
			      role="menuitem"
			      tabIndex="0"
			    >
			      
			    </div>
			    <img src='./Event-Hall.jpg' alt='title or description'/>
			   
			    <img src='./Karaoke-Bar.jpg' alt='title or description'/>
			    <img src='./Horizon-Cafe.jpg' alt='title or description'/>
			    <img src='./Quikmart.jpg' alt='title or description'/>
			    
			    <img src='./Salon.jpg' alt='title or description'/>
			    <img src='./Suite-Room.jpg' alt='title or description'/>
			  </Coverflow>, 
			 </div>
		<Footer/>
		</React.Fragment>
	)
}

export default Accomodation