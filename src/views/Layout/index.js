import Navbar from './Navbar';
import NavbarMain from './NavbarMain';
import Footer from './Footer';
import Landing from './Landing';
import Home1 from './Home1';

export {
	Navbar,
	Footer,
	NavbarMain,
	Landing,
	Home1
}