import React from 'react';
import {Link} from 'react-router-dom';

// const handleLogout = () => {
//     sessionStorage.clear()
//     window.location.replace('#/login')
//   }
  
const NavbarMain = () => {
	return (
		<React.Fragment>
			<nav class="navbar navbar-expand-lg">
			  <p class="blueship navbar-brand" >TravGo</p>
			
			  <button class="navbar-toggler" 
				  type="button" 
				  data-toggle="collapse" 
				  data-target="#navbarColor01" 
				  aria-controls="navbarColor01" 
				  aria-expanded="false" 
				  aria-label="Toggle navigation">
			    <span class="navbar-toggler-icon"></span>
			  </button>

			  <div class="collapse navbar-collapse" id="navbarColor01">
			    <ul class="navbar-nav ml-auto">
			      
			      <li class="nav-item">
			        <Link to="/register"
			        	className="text-white"
			        >Register</Link>
			      </li>
			      <li class="nav-item">
			        <Link to="/login"
			        	className="text-white"
			        >Login</Link>
			      </li>
			      
			    </ul>
			    
			  </div>
			</nav>
		</React.Fragment>
	)
}

export default NavbarMain;