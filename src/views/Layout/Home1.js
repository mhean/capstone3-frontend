import React from 'react';
import {Navbar,Footer} from '../Layout';
// import {Link} from 'react-router-dom';
// import { Player } from 'video-react';

const Home1 =()=>{
    
        return (
           <React.Fragment>
      <Navbar />
        <div className="homepage d-flex justify-content-center align-items-center" style={{height:"95vh"}}>
          <h1 className="homepage-text">WELCOME TO TravGo</h1>     
        </div>
        <div className="d-flex justify-content-center align-items-center">        
          <h5 className="homepage-text2">"You are guaranteed an enjoyable and safe journey"</h5>        
        </div>
      <Footer/>
    </React.Fragment>
        )
    
}
export default Home1;